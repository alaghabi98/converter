#!/bin/bash

# Install pptx2md from GitHub
npm install -g git+https://github.com/dfinke/pptx2md.git

# Convert HTML to Markdown using pptx2md
pptx2md "$PPTX_FILE" > converted_files/output.md
