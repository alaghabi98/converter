#!/bin/bash

# Install pandoc if not already installed
# Uncomment the next line if needed
# npm install -g reveal-md

# Convert PPTX to HTML using reveal-md
reveal-md input.pptx --static output_html/
